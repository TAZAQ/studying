-- вывести все менеджеров, их зарплату и отдел
SELECT
    emp.first_name,
    emp.last_name,
    emp.salary,
    dep.name AS department_name
FROM
    employee emp
    JOIN department dep USING ( department_id )
    JOIN job ON emp.job_id = job.job_id
WHERE
    job.function = 'MANAGER';
    
-- вывести среднюю зп менеджера
SELECT
    avg(emp.salary)
FROM
    employee emp
    JOIN job ON emp.job_id = job.job_id
WHERE
    job.function = 'MANAGER';

-- и отдел, в котором менеджеров больше всего

SELECT NAME
FROM (
    SELECT
        dep.name,
        COUNT(emp.employee_id) as cnt
    FROM
        department   dep
        JOIN employee     emp USING ( department_id )
        JOIN job USING ( job_id )
    WHERE
        job.function = 'MANAGER'
    GROUP BY
        dep.name
)
WHERE cnt = (
    SELECT max(cnt) FROM (
        SELECT
            dep.name,
            COUNT(emp.employee_id) as cnt
        FROM
            department   dep
            JOIN employee     emp USING ( department_id )
            JOIN job USING ( job_id )
        WHERE
            job.function = 'MANAGER'
        GROUP BY
            dep.name
    )
);





