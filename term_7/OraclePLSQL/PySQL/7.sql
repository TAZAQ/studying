SELECT P.DESCRIPTION as Item,
       I2.QUANTITY as Amount,
       I2.ACTUAL_PRICE as Price,
       S_O.ORDER_DATE as OrderDate,
       S_O.SHIP_DATE as ShipDate,
       E.FIRST_NAME || ' ' || E.MIDDLE_INITIAL || '. ' || E.LAST_NAME  as SalerContact,
       D.NAME as Saler,
       C2.NAME as Custumer,
       C2.CITY || ', ' || C2.ADDRESS as CustumerAddress,
       C2.PHONE_NUMBER as CustumerPhone,
       S_O.TOTAL as Total

FROM
     sales_order S_O
         JOIN CUSTOMER C2 on S_O.CUSTOMER_ID = C2.CUSTOMER_ID
         JOIN EMPLOYEE E on C2.SALESPERSON_ID = E.EMPLOYEE_ID
         JOIN DEPARTMENT D on E.DEPARTMENT_ID = D.DEPARTMENT_ID
         JOIN ITEM I2 on S_O.ORDER_ID = I2.ORDER_ID
         JOIN PRODUCT P on I2.PRODUCT_ID = P.PRODUCT_ID
WHERE
      S_O.order_id = &id
