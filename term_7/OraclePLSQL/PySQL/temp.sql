SELECT *
FROM (
         SELECT fname,
                NVL(Sum(SO.TOTAL), '0') total_sum
         FROM (
                  SELECT eid, fname
                  FROM (
                           SELECT EMPLOYEE_ID as eid, FIRST_NAME as fname, HIRE_DATE, ROWNUM row_n
                           FROM (
                                    SELECT EMPLOYEE_ID, FIRST_NAME, HIRE_DATE
                                    FROM EMPLOYEE
                                    WHERE JOB_ID = 670
                                    ORDER BY HIRE_DATE DESC
                                )
                           ORDER BY HIRE_DATE DESC
                       )
                  WHERE row_n <= 5
              )
                  JOIN CUSTOMER on SALESPERSON_ID = eid
                  FULL JOIN SALES_ORDER SO on CUSTOMER.CUSTOMER_ID = SO.CUSTOMER_ID
         GROUP BY fname
     )
WHERE NVL(fname, '-1') != '-1'