SELECT
    dep.name,
    emp.salary
FROM
    department dep
    JOIN employee emp ON dep.department_id = emp.department_id
WHERE
    emp.salary in (
        (
            SELECT
                MIN(emp.salary)
            FROM
                employee emp
        ),
        (
            SELECT
                MAX(emp.salary)
            FROM
                employee emp
        )
    )
GROUP BY
    dep.name,
    emp.salary;