DROP TABLE temp_4;

CREATE TABLE temp_4
    AS
        SELECT
            name,
            state,
            COUNT(order_id) cnt
        FROM
            customer      c
            JOIN sales_order   s_o USING ( customer_id )
        GROUP BY
            name,
            state
;

SELECT
    name,
    state
FROM
    temp_4 t1
WHERE
    cnt = (
        SELECT
            MAX(cnt)
        FROM
            temp_4 t2
        WHERE
            t1.state = t2.state
    )
;

DROP TABLE temp_4;