SELECT fullName,
       years || ' лет ' || months || ' месяцев' as experince,
       CASE
        WHEN yearGroup >= 0*step AND yearGroup <= 1*step THEN 'от ' || 0*step || ' до ' || 1*step
        WHEN yearGroup >= 1*step AND yearGroup <= 2*step THEN 'от ' || 1*step || ' до ' || 2*step
        WHEN yearGroup >= 2*step AND yearGroup <= 3*step THEN 'от ' || 2*step || ' до ' || 3*step
        WHEN yearGroup >= 3*step AND yearGroup <= 4*step THEN 'от ' || 3*step || ' до ' || 4*step
        WHEN yearGroup >= 4*step AND yearGroup <= 5*step THEN 'от ' || 4*step || ' до ' || 5*step
        WHEN yearGroup >= 5*step AND yearGroup <= 6*step THEN 'от ' || 5*step || ' до ' || 6*step
        WHEN yearGroup >= 6*step AND yearGroup <= 7*step THEN 'от ' || 6*step || ' до ' || 7*step
        WHEN yearGroup >= 7*step AND yearGroup <= 8*step THEN 'от ' || 7*step || ' до ' || 8*step
        WHEN yearGroup >= 8*step AND yearGroup <= 9*step THEN 'от ' || 8*step || ' до ' || 9*step
        WHEN yearGroup >= 9*step AND yearGroup <= 10*step THEN 'от ' || 9*step || ' до ' || 10*step
       END as yearRange
FROM (
         SELECT fullName,
                years,
                months,
                years + CEIL(months / 12) as yearGroup,
                &step as step
         FROM (
                  SELECT E.FIRST_NAME || ' ' || E.MIDDLE_INITIAL || '. ' || E.LAST_NAME as fullName,
                         TRUNC(MONTHS_BETWEEN(SYSDATE, E.HIRE_DATE) / 12.0) as years,
                         (TRUNC(MONTHS_BETWEEN(SYSDATE, E.HIRE_DATE)) - TRUNC(MONTHS_BETWEEN(SYSDATE, E.HIRE_DATE) / 12) * 12) as months
                  FROM EMPLOYEE E
                  WHERE E.DEPARTMENT_ID = &depId
              )
     )
ORDER BY
         yearRange
