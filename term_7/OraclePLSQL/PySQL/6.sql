DROP TABLE temp_6;

COL tree FORMAT a20

CREATE TABLE temp_6
AS
    (SELECT
        lpad(' ',(level - 1) * 3)
        || ' '
        || e.last_name || ' ' || e.first_name AS tree,
        j.function,
        e.department_id AS dep_id
    FROM
        employee   e,
        job        j
    WHERE
        e.job_id = j.job_id
    CONNECT BY
        PRIOR e.employee_id = e.manager_id
    START WITH e.last_name = 'KING')
;

SELECT 
    tree as position_name,
    function as position
FROM 
    temp_6;

DROP TABLE temp_6;