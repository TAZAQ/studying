DROP TABLE temp_5;

CREATE TABLE temp_5
    AS
        SELECT
            name,
            max(s_o.total) ttl,
            min(i.total) mit
        FROM
            customer           c
            JOIN sales_order   s_o 
                USING ( customer_id )
            JOIN item          i 
                on s_o.order_id = i.order_id
        GROUP BY
            name
;

SELECT * FROM temp_5;


DROP TABLE temp_5;