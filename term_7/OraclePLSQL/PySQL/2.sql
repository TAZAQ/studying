SELECT
    job.function, dep.department_id, dep.name
FROM
    job
    JOIN employee     emp USING ( job_id )
    JOIN department   dep ON emp.department_id = dep.department_id
    
WHERE
    emp.salary in (
        SELECT max(salary)
        FROM employee emp
        GROUP BY emp.department_id
    )

GROUP BY dep.department_id, job.function, dep.name