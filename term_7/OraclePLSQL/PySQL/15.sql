SELECT SO.ORDER_ID                                                     as "ORDER",
       C2.NAME                                                         as "CUSTUMER",
       E.FIRST_NAME || ' ' || E.LAST_NAME                              as "WORKER",
       E.SALARY,
       ROUND(SO.TOTAL * 0.113748576182, 2)                             as "ORDER_BONUS",
       TRUNC(SO.TOTAL * 0.113748576182 / E.SALARY * 100) ||
       RPAD(MOD(SO.TOTAL * 0.113748576182 / E.SALARY * 100, 1), 21, 0) as "BONUS(%)"

FROM SALES_ORDER SO
         JOIN CUSTOMER C2 on SO.CUSTOMER_ID = C2.CUSTOMER_ID
         JOIN EMPLOYEE E on C2.SALESPERSON_ID = E.EMPLOYEE_ID
WHERE SO.SHIP_DATE - SO.ORDER_DATE <= 3