﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataBindingLab
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static ObservableCollection<Person> persons;
        public string FirstName;
        public string LastName;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            persons = new ObservableCollection<Person>()
            {
                new Person("Иванов", "Иван", "Иванович", 22),
                new Person("Сидоров", "Степан", "Иванович", 25),
                new Person("Петров", "Станислав", "Сидорович", 40)
            };
            this.DataContext = persons;
        }

        private void xAddBtn_Click(object sender, RoutedEventArgs e)
        {
            Person person = new Person();
            SecondWindow secondWindow = new SecondWindow(person, true);
            secondWindow.Owner = this;
            secondWindow.ShowDialog();

            if (person.age != 0 )
            {
                persons.Add(person);
            }
        }

        private void xEditBtn_Click(object sender, RoutedEventArgs e)
        {
            SecondWindow secondWindow = new SecondWindow(xUserList.SelectedItem, true);
            secondWindow.Owner = this;
            secondWindow.ShowDialog();
        }

        private void xViewBtn_Click(object sender, RoutedEventArgs e)
        {
            SecondWindow secondWindow = new SecondWindow(xUserList.SelectedItem, false);
            secondWindow.Owner = this;
            secondWindow.ShowDialog();
        }

        private void xDeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            Person person = xUserList.SelectedItem as Person;
            string str = string.Format("Удалить запись: {0} {1}", person.secondName, person.firstName);

            if ( MessageBox.Show(this, str, "Внимание", MessageBoxButton.YesNo) == MessageBoxResult.Yes )
            {
                persons.Remove(person);
            }
        }

    }
}
