﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataBindingLab
{
    /// <summary>
    /// Логика взаимодействия для SecondWindow.xaml
    /// </summary>
    public partial class SecondWindow : Window
    {
        private Person _tmpPerson;
        private Person _curPerson;

        public SecondWindow()
        {
            InitializeComponent();
        }

        public SecondWindow(object obj, bool isEdit) : this()
        {
            _curPerson = obj as Person;
            this.DataContext = _curPerson;

            if ( isEdit )
            {
                _tmpPerson = new Person(_curPerson);
            }
            else
            {
                xCancelButton.Visibility = Visibility.Collapsed;
                xLastName.IsEnabled = isEdit;
                xFirstName.IsEnabled = isEdit;
                xThirdName.IsEnabled = isEdit;
                xAge.IsEnabled = isEdit;
            }
        }

        private void xOKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void xCancelButton_Click(object sender, RoutedEventArgs e)
        {
            Person.CopyData(_curPerson, _tmpPerson);
            this.Close();
        }
    }
}
