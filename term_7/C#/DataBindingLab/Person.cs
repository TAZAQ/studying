﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBindingLab
{
    public class Person : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _firstName;

        public string firstName { 
            get { return _firstName; }
            set
            {
                if (value != _firstName) 
                {
                    _firstName = value;
                    OnPropertyChanged("firstName");
                }
            } 
        }
        public string secondName { get; set; }
        public string thirdName { get; set; }
        public int age { get; set; }

        public Person()
        {
            firstName = "Фамилия";
            secondName = "Имя";
            thirdName = "Отчество";
            age = 0;
        }

        public Person(string firstName, string secondName, string thirdName, int age)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.thirdName = thirdName;
            this.age = age;
        }

        public Person(Person person)
        {
            this.firstName = person.firstName;
            this.secondName = person.secondName;
            this.thirdName = person.thirdName;
            this.age = person.age;
        }


        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static void CopyData(Person p1, Person p2)
        {
            p1.age = p2.age;
            p1.firstName = p2.firstName;
            p1.secondName = p2.secondName;
            p1.thirdName = p2.thirdName;
        }
    }

}
