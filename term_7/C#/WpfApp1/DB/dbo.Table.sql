﻿CREATE TABLE [dbo].[Phones]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Title] VARCHAR(50) NOT NULL, 
    [Company] VARCHAR(50) NOT NULL, 
    [Price] INT NOT NULL
)
