﻿CREATE PROCEDURE [dbo].[sp_InsertPhone]
	@title varchar(50),
	@company varchar(50),
	@price int,
	@Id int out
AS
	INSERT INTO Phones(Title, Company, Price)
	VALUES(@title, @company, @price)
	SET @Id=SCOPE_IDENTITY()
GO
