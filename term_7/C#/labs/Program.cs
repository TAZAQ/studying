﻿using System;

namespace labs
{
    class Program
    {
        static void Main(string[] args)
        {
            do {
                Console.WriteLine("Введи номер задания, 0 - выход");
                var option = int.Parse(Console.ReadLine());

                if (option == 0) {
                    break;
                } else {
                    switch (option) {
                        case 1: {
                            var task = new Task_1();
                            task.processing();
                            break;
                        }
                        case 2: {
                            var task = new Task_2();
                            task.processing();
                            break;
                        }
                        case 3: {
                            var task = new Task_3();
                            task.processing();
                            break;
                        }
                        case 4: {
                            var task = new Task_4();
                            task.processing();
                            break;
                        }
                        case 5: {
                            var task = new Task_5();
                            task.processing();
                            break;
                        }
                        case 6: {
                            var task = new Task_6();
                            task.processing();
                            break;
                        }
                        case 7: {
                            var task = new Task_7();
                            task.processing();
                            break;
                        }
                        case 8: {
                            var task = new Task_8();
                            task.processing();
                            break;
                        }
                    }
                }

            } while (true);
        }
    }
}