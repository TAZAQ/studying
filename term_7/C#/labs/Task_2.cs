using System;

namespace labs
{
    public class Task_2
    {
        public void processing() {
            Console.WriteLine("Площадь равностороннего треугольника, периметр которого равен p; ");
            Console.WriteLine("Введи периметр");
            var p = double.Parse(Console.ReadLine());
            var s = p/3.0 * Math.Sqrt(3) / 4.0;
            Console.WriteLine("S = {0}", s);
        }
    }
}
