using System;

namespace labs
{
    public class Task_5
    {
        public void processing() {
            Console.WriteLine("Дан номер масти m (1 <= m <= 4), определить название масти. Масти нумеруются: «пики» - 1, «трефы» - 2, «бубны» - 3, «червы» - 4.");
            Console.WriteLine("Введи m");
            var m = int.Parse(Console.ReadLine());
            switch (m) {
                default: {
                    Console.WriteLine("Хз -___-");
                    break;
                }
                case 1: {
                    Console.WriteLine("Пики");
                    break;
                }
                case 2: {
                    Console.WriteLine("Трефы");
                    break;
                }
                case 3: {
                    Console.WriteLine("Бубны");
                    break;
                }
                case 4: {
                    Console.WriteLine("Червы");
                    break;
                }

            }
        }
    }
}
