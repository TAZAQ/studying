using System;

namespace labs
{
    public class Task_7
    {
        public void processing() {
            var rows = 4;
            var cols = 6;
            for (var i = 0; i < rows; i++) {
                for (var j = 0; j < cols; j++) {
                    Console.Write("{0,4}", 5);
                }
                Console.WriteLine("");
            }
        }
    }
}
