using System;

namespace labs
{
    public class Task_1
    {
        public void processing() {
            Console.WriteLine("Как тебя зовут?");
            var _name = Console.ReadLine();
            Console.WriteLine("Сколько тебе?");
            var _age = int.Parse(Console.ReadLine());
            Console.WriteLine("{0}, ты {1} года рождения!", _name, 2019-_age);
        }
    }
}
