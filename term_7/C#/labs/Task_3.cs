using System;

namespace labs
{
    public class Task_3
    {
        public void processing() {
            Console.WriteLine("Кратна ли трем сумма цифр двухзначного числа; ");
            Console.WriteLine("Введи число");
            var number = int.Parse(Console.ReadLine());

            if (number % 3 == 0) {
                Console.WriteLine("Да");
            } else {
                Console.WriteLine("Нет");
            }
        }
    }
}
