using System;

namespace labs
{
    public class Task_4
    {
        public void processing() {
            Console.WriteLine("Дана точка на плоскости с координатами (х, у). Составить программу, которая выдает одно из сообщений «Да», «Нет»");
            Console.WriteLine("Введи X");
            var x = int.Parse(Console.ReadLine());
            Console.WriteLine("Введи Y");
            var y = int.Parse(Console.ReadLine());

            if (x > 0 && x*x + y*y <= 9*9) {
                Console.WriteLine("Да");
            } else {
                Console.WriteLine("Нет");
            }
        }
    }
}
