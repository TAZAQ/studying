using System;

namespace labs
{
    public class Task_8
    {
        public void processing() {
            Console.WriteLine("Построить таблицу значений функции y=f(x) для x in [a, b]  с шагом h");
            Console.WriteLine("Введи a");
            var a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введи b");
            var b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введи h");
            var h = double.Parse(Console.ReadLine());

            var dx = a;

            Console.WriteLine("{0,8:F4}  |{1,8:F4}", "x", "y");
            Console.WriteLine("--------------------");
            while (dx < b) {
                var y = 0.0;
                if (dx >= 0.9) {
                    y = 1.0 / (0.1 + dx) / (0.1 + dx);
                } else if (dx >= 0 && dx <= 0.9) {
                    y = 0.2*dx + 0.1;
                } else {
                    y = dx*dx + 0.2;
                }
                Console.WriteLine("{0,8:F4}  |{1,8:F4}", dx, y);
                dx += h;
            }
        }
    }
}
