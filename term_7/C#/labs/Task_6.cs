using System;

namespace labs
{
    public class Task_6
    {
        public void processing() {
            Console.WriteLine("все целые числа из диапазона от А до В (А<В), оканчивающиеся на цифру Х;");
            Console.WriteLine("Введи A");
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введи B");
            var b = int.Parse(Console.ReadLine());
            Console.WriteLine("Введи X");
            var x = int.Parse(Console.ReadLine());

            for (var i = a; i <= b; i++) {
                if (i % 10 == x) {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
