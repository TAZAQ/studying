using System;

namespace labs2
{
    class Lab2_3
    {

        private void swapRows(float[,] arr, int n, int first, int second) {
            for (var j = 0; j < n; j++){
                var temp = arr[first, j];
                arr[first, j] = arr[second, j];
                arr[second, j] = temp;
            }
        }

        public void processing() {
            Console.WriteLine("Дан двумерный массив размерностью n×n из действительных чисел");
            Console.WriteLine("Поменять местами две средних строки, если количество строк четное, и первую со средней строкой, если количество строк нечетное");

            GeneralFunctions gfunc = new GeneralFunctions();

            Console.WriteLine("Введите n");
            var n = int.Parse(Console.ReadLine());

            var arr = gfunc.generateFloatMatrix(n);
            gfunc.printMatrix(arr);
            if (n % 2 == 0)
                swapRows(arr, n, (int) n / 2, (int) n / 2 - 1 );
            else
                swapRows(arr, n, (int) n / 2, 0);
            gfunc.printMatrix(arr);

        }
    }
}
