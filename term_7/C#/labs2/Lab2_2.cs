using System;

namespace labs2
{
    class Lab2_2
    {
        public void processing() {
            Console.WriteLine("Найти min положительно");
            GeneralFunctions gfunc = new GeneralFunctions();
            var arr = gfunc.generateFloatArray(15);
            gfunc.printArray(arr);

            float min = arr[0];

            foreach (float x in arr) {
                if (min <= 0 && x > 0 || x > 0 && x < min) min = x;
            }

            Console.WriteLine("positive min = {0}", min);
        }
    }
}
