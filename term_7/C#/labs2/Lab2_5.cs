using System;

namespace labs2
{
    class Lab2_5
    {

        private int getEnterCount(String str, String sym) {
            int count = 0;
            String low_str = str.ToLower();
            String low_sym = sym.ToLower();

            for (var i = 0; i < str.Length; i++) {
                if (str[i].ToString() == sym) {
                    count++;
                }
            }

            return count;
        }

        public void processing() {
            Console.WriteLine("Подсчитывает общее число вхождений символов х и y");
            String fishText = "С другой стороны дальнейшее развитие различных форм деятельности играет важную роль в формировании модели развития. " +
                                "Разнообразный и богатый опыт консультация с широким активом обеспечивает широкому кругу " +
                                " (специалистов) участие в формировании системы обучения кадров, соответствует насущным потребностям.";
            Console.WriteLine(fishText);
            Console.WriteLine("");

            Console.WriteLine("Введите X");
            String x = Console.ReadLine();
            Console.WriteLine("Введите Y");
            String y = Console.ReadLine();

            Console.WriteLine("Символов '{0}' в fishText = {1}", x, getEnterCount(fishText, x));
            Console.WriteLine("Символов '{0}' в fishText = {1}", y, getEnterCount(fishText, y));

        }
    }
}
