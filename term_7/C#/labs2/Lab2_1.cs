using System;

namespace labs2
{
    class Lab2_1
    {
        public void processing() {
            Console.WriteLine("Дан одномерный массив из 15 целых чисел, Заменить все элементы, попадающие в интервал [a, b], нулем ");
            GeneralFunctions gfunc = new GeneralFunctions();
            int[] arr = gfunc.generateIntArray(15);
            gfunc.printArray(arr);

            Console.WriteLine("Write a");
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine("Write b");
            var b = int.Parse(Console.ReadLine());

            for (var i = 0; i < arr.Length; i++) {
                if (arr[i] >= a && arr[i] <= b) {
                    arr[i] = 0;
                }
            }

            gfunc.printArray(arr);
        }
    }
}
