using System;

namespace labs2
{
    class GeneralFunctions
    {
        public int[] generateIntArray(int len) {
            int[] generatedArray = new int[len];
            Random random = new Random();

            for (var i = 0; i < len; i++) {
                generatedArray[i] = random.Next(-100, 100);
            }

            return generatedArray;
        }

        public float[] generateFloatArray(int len) {
            float[] generatedArray = new float[len];
            Random random = new Random();

            for (var i = 0; i < len; i++) {
                generatedArray[i] = (float) (random.NextDouble()-0.5) * 100;
            }

            return generatedArray;
        }

        public float[,] generateFloatMatrix(int n) {
            float[,] generatedArray = new float[n, n];
            Random random = new Random();

            for (var i = 0; i < n; i++)
                for (var j = 0; j < n; j++)
                    generatedArray[i, j] = (float) (random.NextDouble()-0.5) * 100;
            return generatedArray;
        }

        public int[][] generateStepIntArray(int n) {
            int[][] generatedArray = new int[n][];
            Random random = new Random();
            for (var i = 0; i < n; i++) {
                var r = random.Next(0, n);
                generatedArray[i] = generateIntArray(r);
            }

            return generatedArray;
        }

        public void printArray(int[] arr) {
            for (var i = 0; i < arr.Length; i++) {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine("");
        }

        public void printArray(float[] arr) {
            for (var i = 0; i < arr.Length; i++) {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine("");
        }

        public void printMatrix(float[,] arr) {
            int n = (int) Math.Sqrt(arr.Length);
            for (var i = 0; i < n; i++) {
                for (var j = 0; j < n; j++)
                    Console.Write("{0,7:F2} ", arr[i, j]);
                Console.WriteLine("");
            }
        }

        public void printStepArray(int[][] arr) {
            for (var i = 0; i < arr.Length; i++) {
                for (var j = 0; j < arr[i].Length; j++)
                    Console.Write("{0,7:F2} ", arr[i][j]);
                Console.WriteLine("");
            }
        }
    }
}
