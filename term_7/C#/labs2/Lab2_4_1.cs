using System;

namespace labs2
{
    class Lab2_4_1
    {
        private int findMaxLength(int[][] arr) {
            int max = 0;
            for (var i = 0; i < arr.Length; i++) {
                if (max < arr[i].Length) {
                    max = arr[i].Length;
                }
            }

            return max;
        }

        private int[] FirstPositiveInCols(int[][] arr) {
            int max_len = findMaxLength(arr);
            int[] result = new int[max_len];

            for (var i = 0; i < max_len; i++) {
                result[i] = -1;
            }

            for (var i = 0; i < arr.Length; i++) {
                for (var j = 0; j < arr[i].Length; j++) {
                    if (result[j] == -1 && arr[i][j] > 0) {
                        result[j] = arr[i][j];
                    }
                }
            }

            return result;
        }

        public void processing() {
            Console.WriteLine("Дан ступенчатый массив целых чисел");
            Console.WriteLine("Для каждого столбца найти первый положительный элемент и записать данные в новый массив");

            GeneralFunctions gfunc = new GeneralFunctions();

            var n = 5;
            int[][] arr = gfunc.generateStepIntArray(n);
            gfunc.printStepArray(arr);

            gfunc.printArray(FirstPositiveInCols(arr));

        }
    }
}
