using System;

namespace labs2
{
    class Lab2_4
    {

        private int findMaxLength(int[][] arr) {
            int max = 0;
            for (var i = 0; i < arr.Length; i++) {
                if (max < arr[i].Length) {
                    max = arr[i].Length;
                }
            }

            return max;
        }

        private int[] ColsSummirize(int[][] arr) {
            var max_count = findMaxLength(arr);
            int[] summ_arr = new int[max_count];
            for (var i = 0; i < max_count; i++) {
                summ_arr[i] = 0;
            }

            for (var i = 0; i < arr.Length; i++) {
                for (var j = 0; j < arr[i].Length; j++) {
                    if (arr[i][j] > 0 && arr[i][j] % 2 == 0)
                        summ_arr[j] += arr[i][j];
                }
            }

            return summ_arr;
        }

        public void processing() {
            Console.WriteLine("Дан ступенчатый массив целых чисел");
            Console.WriteLine("Для каждого столбца подсчитать сумму четных положительных элементов и записать данные в новый массив");

            GeneralFunctions gfunc = new GeneralFunctions();

            var n = 5;
            int[][] arr = gfunc.generateStepIntArray(n);
            gfunc.printStepArray(arr);

            int[] line_row = ColsSummirize(arr);
            gfunc.printArray(line_row);

        }
    }
}
