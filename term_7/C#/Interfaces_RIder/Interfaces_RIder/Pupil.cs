﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyInterfaces
{
    class Pupil : PersonalData, EducationData
    {
        public string FIO { get; set; }
        public string DOB { get; set; }
        public string School { get; set; }
        public string EnterDate { get; set; }
        public string GraduateDate { get; set; }

        public string GetEducationData()
        {
            if (School == "" || EnterDate == "" || GraduateDate == "")
            {
                return "Нет данных";
            }

            return string.Format(
                "Ученик {0}, образование - {1}, дата поступления - {2}, дата выпуска {3}", 
                FIO, School, EnterDate, GraduateDate
            );
            // throw new NotImplementedException();
        }

        public string GetNameDOB()
        {
            return string.Format("Ученик {0}, дата рождения: {1}", FIO, DOB);
            // throw new NotImplementedException();
        }

        public Pupil(string fio, string dob, string school, string enterDate, string graduteDate)
        {
            FIO = fio;
            DOB = dob;
            School = school;
            EnterDate = enterDate;
            GraduateDate = graduteDate;
        }

        public Pupil(string fio, string dob) : this(fio, dob, "", "", "") { }
     }
}
