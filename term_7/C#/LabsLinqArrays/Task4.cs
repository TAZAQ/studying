using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task4 : Tasks
    {
        public void processing() {
            int number_count = 15;
            Random random = new Random();
            int[] numbers = new int[number_count];
            for (int i = 0; i < number_count; i++) { numbers[i] = random.Next(101) - 50; }

            for (int i = 0; i < number_count; i++) { Console.Write(numbers[i].ToString() + " "); }
            Console.WriteLine("");

            this.showText("Задание 4. Вывести сумму положительных двузначных элементов");
            var numQuery1 = from number in numbers where (number >= 10 && number <= 99) select number;

            this.showText(numQuery1.Sum(), true);
        }
    }
}