using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task8 : Tasks
    {
        public void processing() {
            int number_count = 15;
            Random random = new Random();
            int[] numbers = new int[number_count];
            for (int i = 0; i < number_count; i++) { numbers[i] = random.Next(1001) - 500; }

            for (int i = 0; i < number_count; i++) { Console.Write(numbers[i].ToString() + " "); }
            Console.WriteLine("");

            this.showText("Задание 8. Дана последовательность целых чисел, вывести все трехзначные числа последовательности в обратном порядке");
            var numQuery1 = from number in numbers where (number >= 100 && number <= 999) select number;

            foreach(int num in numQuery1.Reverse()) {
                showText(num);
            }

            showText("", true);
        }
    }
}