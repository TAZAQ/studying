using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task10 : Tasks
    {
        public void processing() {
            this.showText("Задание 10. вывести Нечетные элементы строковой последовательности");

            string[] stroke = new string[15] { "Мама", "Мыла", "Раму", "Папа",
                "Читал", "Газету", "Кошка", "Катала", "Клубок", "Четные", "элементы",
                "строковой", "последовательности", "Нечетные", "неэлементы"
            };

            var strQuery = stroke
                            .Select((strok, index) => new {strok, index} )
                            .Where(n => n.index % 2 == 0)
                            .Select(n => n.strok)
                            ;

            foreach (string s in strQuery) {
                showText(s);
            }
            showText("", true);
        }
    }
}