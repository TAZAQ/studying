using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task7 : Tasks
    {
        public void processing() {
            this.showText("Задание 7. Дана последовательность строк вывести количество строк, в которых 3-я буква «м»");

            string[] stroke = new string[9] { "Мама", "Мыла", "Раму", "Папа", "Читал", "Газету", "Кошка", "Катала", "Клубок" };

            var strQuery = from strok in stroke where (strok.IndexOf("м") == 2) select strok;
            showText(strQuery.Count(), true);
        }
    }
}