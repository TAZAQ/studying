using System;

namespace LabsLinqArrays
{
    public class Tasks
    {
    public void showText(string text) {
        Console.WriteLine(text);
    }

    public void showText(int num) {
        Console.WriteLine(num.ToString());
    }

    public void showText(string text, bool isEnd=false) {
        Console.WriteLine(text);
        if (isEnd) Console.WriteLine("*****************************************************************************************************");
    }

    public void showText(int num, bool isEnd=false) {
        Console.WriteLine(num.ToString());
        if (isEnd) Console.WriteLine("*****************************************************************************************************");
    }

    }
}