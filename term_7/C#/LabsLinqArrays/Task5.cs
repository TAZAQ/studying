using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task5 : Tasks
    {
        public void processing() {
            int number_count = 15;
            Random random = new Random();
            int[] numbers = new int[number_count];
            for (int i = 0; i < number_count; i++) { numbers[i] = random.Next(101) - 50; }

            for (int i = 0; i < number_count; i++) { Console.Write(numbers[i].ToString() + " "); }
            Console.WriteLine("");

            this.showText("Задание 5. Дана цифра D (однозначное целое число) и целочисленная последовательность A.");
            this.showText("Вывести первый положительный элемент последовательности A, оканчивающийся цифрой D.");
            this.showText("Если требуемых элементов в последовательности A нет, то вывести 0");
            int D = 1;
            var numQuery1 = from number in numbers where (number > 0 && number.ToString().EndsWith(D.ToString())) select number;

            if (numQuery1.Count() != 0) this.showText(numQuery1.First(), true);
            else this.showText("0", true);
        }
    }
}