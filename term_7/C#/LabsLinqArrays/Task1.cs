using System;
using System.Linq;

namespace LabsLinqArrays
{
    public class Task1 : Tasks
    {
        public void processing() {
            int number_count = 15;
            Random random = new Random();
            int[] numbers = new int[number_count];
            for (int i = 0; i < number_count; i++) { numbers[i] = random.Next(11) - 5; }

            for (int i = 0; i < number_count; i++) { Console.Write(numbers[i].ToString() + " "); }
            Console.WriteLine("");

            this.showText("Задание 1. Первый положительный элемент");
            var numQuery1 = from number in numbers where (number > 0) select number;
            this.showText(numQuery1.First(), true);
        }
    }
}