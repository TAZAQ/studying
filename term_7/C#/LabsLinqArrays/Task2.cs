using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task2 : Tasks
    {
        public void processing() {
            this.showText("Задание 2. Вывести строки, начинающиеся на «М» и имеющие длину 4 символа");

            string[] stroke = new string[9] { "Мама", "Мыла", "Раму", "Папа", "Читал", "Газету", "Кошка", "Катала", "Клубок" };

            var strQuery = from strok in stroke where (strok.Length == 4 && strok.StartsWith("М")) select strok;
            foreach(string s in strQuery) {
                showText(s);
            }

            showText("", true);
        }
    }
}