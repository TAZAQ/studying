﻿using System.Threading.Tasks;
using System;
using System.Linq;

namespace LabsLinqArrays
{
    class Program
    {
        public static void Main(string[] args)
        {
            Task1 task1 = new Task1();
            task1.processing();

            Task2 task2 = new Task2();
            task2.processing();

            Task3 task3 = new Task3();
            task3.processing();

            Task4 task4 = new Task4();
            task4.processing();

            Task5 task5 = new Task5();
            task5.processing();

            Task6 task6 = new Task6();
            task6.processing();

            Task7 task7 = new Task7();
            task7.processing();

            Task8 task8 = new Task8();
            task8.processing();

            Task9 task9 = new Task9();
            task9.processing();

            Task10 task10 = new Task10();
            task10.processing();
        }
    }
}
