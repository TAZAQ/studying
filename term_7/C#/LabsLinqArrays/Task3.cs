using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task3 : Tasks
    {
        public void processing() {
            this.showText("Задание 3. Дана строковая последовательность, состоящая из 10 элементов вывести: Сумму длин всех строк, заканчивающихся на «а»");

            string[] stroke = new string[10] { "Мама", "Мыла", "Раму", "Папа", "Читал", "Газету", "Кошка", "Катала", "Клубок", "Ниток"};

            var strQuery = from strok in stroke where (strok.EndsWith("а")) select (strok, strok.Length);

            foreach( (string s, int s_len) in strQuery) {
                showText(s.ToString() + "  " + s_len.ToString());
            }

            var strQuery1 = from strok in stroke where (strok.EndsWith("а")) select strok.Length;

            showText(strQuery1.Sum(), true);
        }
    }
}