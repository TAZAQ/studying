using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LabsLinqArrays
{
    public class Task6 : Tasks
    {
        public void processing() {
            this.showText("Задание 6. Дана последовательность строк отсортировать последовательность по убыванию длины строки");

            string[] stroke = new string[9] { "Мама", "Мыла", "Раму", "Папа", "Читал", "Газету", "Кошка", "Катала", "Клубок" };

            var strQuery = from strok in stroke orderby strok.Length descending select strok;
            foreach(string s in strQuery) {
                showText(s);
            }

            showText("", true);
        }
    }
}