namespace Lab1
{
    public class Student
    {
        public int Code { get; set; }
        public string Fio { get; set; }
        public string Group { get; set; }
        public bool Gender { get; set; }
        public string Dob { get; set; }
        public float AvgMark { get; set; }
        public int TrainerCode { get; set; }

        public  Student(int code, string fio, string group, bool gender, string dob, float avgMark, int trainerCode) {
            Code = code;
            Fio = fio;
            Group = group;
            Gender = gender;
            Dob = dob;
            AvgMark = avgMark;
            TrainerCode = trainerCode;
        }
    }
}