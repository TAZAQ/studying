using System;
using System.Linq;

namespace Lab1
{
    public class Query7 : Queries
    {
        public void processing(Student[] students, Trainer[] trainers) {
            showText("7. ФИО, группа, ФИО руководителя");

            var query = from student in students
                        join trainer in trainers on student.TrainerCode equals trainer.Code
                        select new {
                            fio = student.Fio,
                            gr = student.Group,
                            trainerFio = trainer.Fio
                        }
            ;

            foreach (var q in query) {
                showText(string.Format("ФИО студента: {0}", q.fio));
                showText(string.Format("Группа: {0}", q.gr));
                showText(string.Format("ФИО руководителя: {0}", q.trainerFio));
                showText(string.Format(""));
            }

            showText("", true);
        }
    }
}