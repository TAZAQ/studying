namespace Lab1
{
    public class Trainer
    {
        public int Code { get; set; }
        public string Fio { get; set; }
        public string Position { get; set; }

        public Trainer(int code, string fio, string position) {
            Code = code;
            Fio = fio;
            Position = position;
        }
    }
}