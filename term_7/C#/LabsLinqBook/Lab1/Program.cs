﻿using System;

namespace Lab1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Trainer[] trainers = new Trainer[3];

            trainers[0] = new Trainer(
                20654,
                "Cecilia Schulist",
                "Customer Creative Representative"
            );

            trainers[1] = new Trainer(
                90159,
                "Cleve Emard",
                "Forward Operations Facilitator"
            );

            trainers[2] = new Trainer(
                50851,
                "Dr. Theodore Ryan",
                "Lead Quality Developer"
            );


            Student[] students = new Student[5];

            students[0] = new Student(
                65238,
                "Irwin Rodriguez",
                "Consultant",
                true,
                "01.02.2003",
                (float) 3.6,
                50851
            );

            students[1] = new Student(
                13701,
                "Brandi Bednar",
                "Assistant",
                true,
                "02.03.2003",
                (float) 4.2,
                90159
            );

            students[2] = new Student(
                19472,
                "Lucio Langosh",
                "Consultant",
                true,
                "04.05.2002",
                (float) 4.7,
                20654
            );

            students[3] = new Student(
                58020,
                "Lucas Donnelly",
                "Analyst",
                true,
                "06.07.2002",
                (float) 3.4,
                50851
            );

            students[4] = new Student(
                58020,
                "Alba Feest",
                "Analyst",
                false,
                "07.12.2002",
                (float) 4.1,
                90159
            );


            Query1 query1 = new Query1();
            query1.processing(students);

            Query2 query2 = new Query2();
            query2.processing(students, "06.07.2002");

            Query3 query3 = new Query3();
            query3.processing(students, (float) 3.7);

            Query4 query4 = new Query4();
            query4.processing(students, "Analyst");

            Query5 query5 = new Query5();
            query5.processing(students, "Analyst");

            Query6 query6 = new Query6();
            query6.processing(students);

            Query7 query7 = new Query7();
            query7.processing(students, trainers);
        }
    }
}
