using System;
using System.Linq;

namespace Lab1
{
    public class Query3 : Queries
    {
        public void processing(Student[] students, float needMark) {
            showText("3. Количество студентов средний балл > " + needMark);

            var query = from student in students where (student.AvgMark > needMark) select student;

            showText(query.Count());

            showText("", true);
        }
    }
}