using System.Linq.Expressions;
using System;

namespace Lab1
{
    public class Queries
    {

    public void showText(string text, bool isEnd=false) {
        Console.WriteLine(text);
        if (isEnd) Console.WriteLine("**************************************************************************************************************************");
    }

    public void showText(int num, bool isEnd=false) {
        Console.WriteLine(num.ToString());
        if (isEnd) Console.WriteLine("**************************************************************************************************************************");
    }

    public void showText(Student student, bool isEnd=false) {
        Console.WriteLine("Код: {0}", student.Code);
        Console.WriteLine("ФИО студента: {0}", student.Fio);
        Console.WriteLine("Пол: {0}", student.Gender);
        Console.WriteLine("Дата рождения: {0}", student.Dob);
        Console.WriteLine("Средний балл: {0}", student.AvgMark);
        Console.WriteLine("Код научного руководителя: {0}", student.TrainerCode);
        Console.WriteLine("");

        if (isEnd) Console.WriteLine("**************************************************************************************************************************");
    }

    }
}