using System;
using System.Linq;

namespace Lab1
{
    public class Query1 : Queries
    {
        public void processing(Student[] students) {
            showText("1. Данные по студентам мужского пола");

            var query = from student in students where (student.Gender) select student;
            
            foreach (Student student in query) {
                showText(student);
            }
            showText("", true);
        }
    }
}