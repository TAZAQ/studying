using System;
using System.Linq;

namespace Lab1
{
    public class Query4 : Queries
    {
        public void processing(Student[] students, string needGroup) {
            showText("4. ФИО и ДР студентов группы " + needGroup);

            var query = from student in students where (student.Group == needGroup) select student;

            foreach (Student student in query) {
                showText("ФИО студента: " + student.Fio);
                showText("ДР студента: " + student.Dob);
                showText("");
            }
            showText("", true);
        }
    }
}