using System;
using System.Linq;

namespace Lab1
{
    public class Query2 : Queries
    {
        public void processing(Student[] students, string dob) {
            showText("2. ФИО студентов с датой рождения == " + dob);

            var query = from student in students where (student.Dob == dob) select student;

            foreach (Student student in query) {
                showText(student.Fio);
            }
            showText("", true);
        }
    }
}