using System;
using System.Linq;

namespace Lab1
{
    public class Query6 : Queries
    {
        public void processing(Student[] students) {
            showText("6. Все студенты по группам ");

            var query = from student in students group student by student.Group;

            foreach (IGrouping<string, Student> student in query) {
                showText(student.Key + ":");
                foreach (var col in student)
                    showText(col.Fio);
                showText("");
            }

            showText("", true);
        }
    }
}