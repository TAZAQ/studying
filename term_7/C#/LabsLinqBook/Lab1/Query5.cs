using System;
using System.Linq;

namespace Lab1
{
    public class Query5 : Queries
    {
        public void processing(Student[] students, string needGroup) {
            showText("5. Общий средний балл для всех студентов группы " + needGroup);

            var query = from student in students where (student.Group == needGroup) select student.AvgMark;

            showText( string.Format("Общий средний балл группы {0} == {1}", needGroup, query.Average()) );

            showText("", true);
        }
    }
}