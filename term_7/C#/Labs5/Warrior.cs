using System;

namespace Labs5
{
    public class Warrior
    {
        private float hp;
        private float def;
        private float dmg;
        private string warrior_name;

        public Warrior(string name, float hp, float def, float dmg) {
            this.name = name;
            this.hp = hp;
            this.def = def;
            this.dmg = dmg;
        }

        public float health_point {
            set { hp = value; }
            get { return hp; }
        }

        public float armor {
            get { return def; }
            set { def = value; }
        }

        public float damage {
            get { return dmg; }
            set { dmg = value; }
        }

        public string name {
            get { return warrior_name; }
            set { warrior_name = value; }
        }

        private void get_damage(Warrior enemy, float damage) {
            float real_damage = damage * (100 - def) / 100;
            health_point -= real_damage;
            Console.WriteLine("'{0}(hp{1,5})' >>> Получен урон ({2}) от {3}", name, health_point, real_damage, enemy.name);
            if (health_point <= 0) {
                Console.WriteLine("'{0}' >>> умер, убийца - {1}", name, enemy.name);
            }
        }

        public void take_damage(Warrior enemy) {
            Console.WriteLine("'{0}(hp{1,5})' >>> Нанесен урон ({2}) по {3}", name, health_point, damage, enemy.name);
            enemy.get_damage(this, damage);
        }
    }
}