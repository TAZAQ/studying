﻿using System;

namespace Labs5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightArmorWarrior law = new LightArmorWarrior("Вася", 100, 10, 10);
            HeavyArmorWarrior haw = new HeavyArmorWarrior("Петя", 150, 20, 5);

            int round = 1;
            while (true) {
                show_round(round++, haw.name);
                haw.take_damage(law);
                if (law.health_point <= 0) break;

                show_round(round++, law.name);
                law.take_damage(haw);
                if (haw.health_point <= 0) break;
            }
        }

        private static void show_round(int round, string name) {
            Console.WriteLine("");
            Console.WriteLine("Ходит '{0}', ход - {1}", name, round);
        }
    }
}
