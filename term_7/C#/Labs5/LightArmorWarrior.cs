namespace Labs5
{
    public class LightArmorWarrior : Warrior
    {
        public LightArmorWarrior (string name, float hp, float def, float dmg) : base(name, hp, def, dmg)
        {
            health_point = hp;
            armor = def;
            damage = dmg;
        }
    }
}