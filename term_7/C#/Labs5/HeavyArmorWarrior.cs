namespace Labs5
{
    public class HeavyArmorWarrior : Warrior
    {
        public HeavyArmorWarrior (string nm, float hp, float def, float dmg) : base(nm, hp, def, dmg)
        {
            name = nm;
            health_point = hp;
            armor = def;
            damage = dmg;
        }
    }
}