﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyInterfaces
{
    class Bachelor : Pupil
    {
        public Bachelor(string fio, string dob) : base(fio, dob) {}

        public Bachelor(
            string fio, string dob, string school, string enterDate, string graduteDate) : base(fio, dob, school, enterDate, graduteDate){}

        public string SpecialityName { get; set; }
    }
}
