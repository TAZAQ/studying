﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyInterfaces
{
    class Master : Bachelor
    {
        private Bachelor firstDegree;

        public Master(string fio, string dob) : base(fio, dob){}
        public Master(string fio, string dob, string school, string enterDate, string graduteDate) : base(fio, dob, school, enterDate, graduteDate){}

        string Speciality { get; set; }

        public Bachelor FirstEducation {
            set { firstDegree = value; }
        }

        public string getFirstEducation()
        {
            return string.Format("Первое образование:\nБакалавр направления {0}, поступил в {1} {2} г., окончил {3} г.",
                firstDegree.SpecialityName, firstDegree.School, firstDegree.EnterDate, firstDegree.GraduateDate    
            );
        }
        
    }
}
