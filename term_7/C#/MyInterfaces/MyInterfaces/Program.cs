﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var bachelor = new Bachelor("Чеснов Владислав Вадимович", "09.05.1995");
            bachelor.SpecialityName = "Информатика и вычислительная техника (бакалавр)";
            bachelor.School = "БФ ПНИПУ";
            bachelor.EnterDate = "01.09.2016";
            bachelor.GraduateDate = "30.06.2020";
            Console.WriteLine(bachelor.GetNameDOB());
            Console.WriteLine(bachelor.GetEducationData());
            Console.WriteLine(bachelor.SpecialityName);

            Console.WriteLine("************************");

            var master = new Master(
                "Чеснов Владислав Вадимович", 
                "09.05.1995", 
                "БФ ПНИПУ",
                "01.09.2020",
                "30.06.2022"
            );
            master.SpecialityName = "Супер информатика и вычислительная техника (маг)";
            master.FirstEducation = bachelor;
            Console.WriteLine(master.GetNameDOB());
            Console.WriteLine(master.GetEducationData());
            Console.WriteLine(master.SpecialityName);
            Console.WriteLine(master.getFirstEducation());
        }
    }
}
