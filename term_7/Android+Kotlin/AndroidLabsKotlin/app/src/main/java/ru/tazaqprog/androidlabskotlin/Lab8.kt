package ru.tazaqprog.androidlabskotlin

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_lab8.*

class Lab8 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab8)
    }

    fun lab8ButtonsClick(view: View) {
        val intent: Intent
        try {
            when (view.id) {
                R.id.lab8btnWeb -> {
                    intent = Intent(this, MyBrowser::class.java)
                    startActivity(intent)
                }
                R.id.lab8btnMap -> {
                    intent = Intent(Intent.ACTION_VIEW);
                    intent.data = Uri.parse(lab8EditText.text.toString())
                    startActivity(intent)
                }
                R.id.lab8btnContacts -> {
                    intent = Intent(Intent.ACTION_VIEW);
                    intent.data = Uri.parse("content://contacts/people/")
                    startActivity(intent)
                }
                R.id.lab8btnCall -> {
                    intent = Intent(Intent.ACTION_DIAL);
                    intent.data = Uri.parse("tel:*105#");
                    startActivity(intent);
                }
            }

        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(this,"Что-то пошло не так", Toast.LENGTH_SHORT).show()
        }
    }
}
