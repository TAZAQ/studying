package ru.tazaqprog.androidlabskotlin

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_lab7_db_add_task.*
import java.lang.Exception

class Lab7_db_work_with_task : AppCompatActivity() {

    var mDatabaseHelper: DBHelper? = null
    var action: String = ""
    var myExtraTask: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab7_db_add_task)
        mDatabaseHelper = DBHelper(this, "mydatabase.db", null, 1)
        action = intent!!.getStringExtra("Action")!!

        when (action) {
            "Add" -> btnAddTask.text = "Добавить"
            "Change" -> btnAddTask.text = "Изменить"
            else -> btnAddTask.text = "Кнопка"
        }

        try {
            myExtraTask = intent!!.getStringExtra("Task")!!
            if (! myExtraTask.isEmpty()) {
                taskText.setText(myExtraTask)
            }
        }
        catch (ex: Exception) {

        }
    }

    fun intentButtonsClick(view: View) {
        when (view.id) {
            R.id.btnAddTask -> {
                when (action) {
                    "Add" -> {
                        DBInsert(taskText.text.toString(), "0")
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    "Change" -> {
                        DBChange(myExtraTask, taskText.text.toString())
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    else -> Toast.makeText(this, "Ничего не знаю!", Toast.LENGTH_LONG).show()
                }
            }

            R.id.btnCancelTask -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
    }

    fun DBInsert(mtask: String, done: String) {
        mDatabaseHelper!!.addTask(mtask, done)
    }

    fun DBChange(oldTaskName: String, newTaskName: String) {
        mDatabaseHelper!!.changeTaskName(oldTaskName, newTaskName)
    }
}
