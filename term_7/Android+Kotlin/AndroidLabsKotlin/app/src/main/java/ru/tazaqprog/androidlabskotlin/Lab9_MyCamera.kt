package ru.tazaqprog.androidlabskotlin

import android.hardware.Camera
import android.hardware.Camera.CameraInfo
import android.hardware.Camera.PictureCallback
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lab9__my_camera.*
import java.io.File
import java.io.FileOutputStream


class Lab9_MyCamera : AppCompatActivity(), SurfaceHolder.Callback {

    lateinit var camera: Camera
    lateinit var surfaceHolder: SurfaceHolder
    var photoFile: File? = null
    var videoFile: File? = null
    var directory: File? = null
    private lateinit var mediaRecorder: MediaRecorder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab9__my_camera)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        surfaceHolder = lab9_surfaseView.holder
        surfaceHolder.addCallback(this)
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)


        //overlay
        val inflater = LayoutInflater.from(baseContext)
        val overlay = inflater.inflate(R.layout.overlay, null)
        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )
        addContentView(overlay, params)

        // overlay buttons recording
        directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        photoFile = File(directory, "myphoto.jpg")
        videoFile = File(directory, "myvideo.mp4")

    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        camera.stopPreview()
        setCameraDisplayOrientation()
        try {
            camera.setPreviewDisplay(holder)
            camera.startPreview()
        } catch (ex: Exception) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        surfaceHolder.removeCallback(this)
        camera.release()
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        try {
            camera = Camera.open(0)
            camera.setPreviewDisplay(surfaceHolder)
            camera.startPreview()
        }
        catch (ex: Exception) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    fun setCameraDisplayOrientation() {
        val rotation = windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }
        var result = 0
        val info = CameraInfo()
        Camera.getCameraInfo(0, info)
        result = (360 - degrees + info.orientation) % 360
        camera.setDisplayOrientation(result)
    }

    fun onStart(view: View) {
        if (prepareVideoRecorder()) {
            mediaRecorder.start();
        } else {
            releaseMediaRecorder();
        }
    }
    fun onStop(view: View) {
        mediaRecorder.stop();
        releaseMediaRecorder();
    }
    fun onTake(view: View) {
        camera.takePicture(null, null, PictureCallback { data, camera ->
            try {
                val fos = FileOutputStream(photoFile)
                fos.write(data)
                fos.close()
                camera.stopPreview()
                camera.setPreviewDisplay(surfaceHolder)
                camera.startPreview()
            } catch (e: java.lang.Exception) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun prepareVideoRecorder(): Boolean {
        camera.unlock()
        mediaRecorder = MediaRecorder()
        mediaRecorder.setCamera(camera)
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER)
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA)
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH))
        mediaRecorder.setOutputFile(videoFile!!.absolutePath)
        mediaRecorder.setPreviewDisplay(lab9_surfaseView.getHolder().getSurface())
        try {
            mediaRecorder.prepare()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
            releaseMediaRecorder()
            return false
        }
        return true
    }

    private fun releaseMediaRecorder() {
        mediaRecorder.reset()
        mediaRecorder.release()
        camera.lock()
    }

}
