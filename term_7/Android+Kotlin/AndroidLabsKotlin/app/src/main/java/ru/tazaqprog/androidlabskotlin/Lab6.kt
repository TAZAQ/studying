package ru.tazaqprog.androidlabskotlin

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_tools.*

class Lab6 : AppCompatActivity() {
    lateinit var mySettings: SharedPreferences
    lateinit var myEditor: SharedPreferences.Editor
    private val APP_SETTINGS = "settings"

    private lateinit var myAuth: MyAuthorityService

    private lateinit var appBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab6)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_tools, R.id.nav_share
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        mySettingsInit()
    }

    private fun mySettingsInit() {
        mySettings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE)
        myEditor = mySettings.edit()
        myAuth = MyAuthorityService(mySettings, myEditor)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.lab6, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun alertDialogActions() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Но Вы только пришли =(")
            .setMessage("Вы действительно хотите выйти?")
            .setNegativeButton("Нет") { dialog, which -> dialog.cancel() }
            .setPositiveButton("ДА") { dialog, which -> finish() }

        val alert = builder.create()
        alert.show()
    }

    override fun onBackPressed() {
        alertDialogActions()
    }

    fun buttonChangePassword(view: View) {
        val oldPassword = tvOldPassword.text.toString()
        val newPassword = tvNewPassword.text.toString()
        val newPasswordConfirm = tvNewPasswordConfirm.text.toString()

        Toast.makeText(this,
            myAuth.changePassword(oldPassword, newPassword, newPasswordConfirm),
            Toast.LENGTH_SHORT)
            .show()

        tvOldPassword.text.clear()
        tvNewPassword.text.clear()
        tvNewPasswordConfirm.text.clear()
    }

    fun animationButtonsClick(view: View) {
        val imageView: ImageView = findViewById(R.id.myImageViewer)
        val btnStart = findViewById<View>(R.id.l6btnStart) as Button
        val btnStop = findViewById<View>(R.id.l6btnStop) as Button
        val seekBar = findViewById<View>(R.id.seekBar) as SeekBar

        imageView.setBackgroundResource(R.drawable.pupil)
        val animationDrawable = imageView.background as AnimationDrawable

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                animationDrawable.alpha = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        when (view.id) {
            R.id.l6btnStart -> animationDrawable.start()
            R.id.l6btnStop -> animationDrawable.stop()
        }


    }


}
