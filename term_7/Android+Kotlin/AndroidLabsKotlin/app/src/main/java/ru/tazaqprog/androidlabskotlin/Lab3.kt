package ru.tazaqprog.androidlabskotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_lab3.*
import java.lang.Exception
import kotlin.math.tan

class Lab3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab3)
    }

    fun btnClear(view: View) {
        variable.setText("")
        answer.text = ""
    }
    fun btnSolution(view: View) {
        val text: String = variable.text.toString()
        var x: Double

        try {
            x = text.toDouble()
        } catch (e: Exception) {
            x = 0.0
        }
        answer.text = (x*x*x*x*x/tan(2*x*x*x)).toString()
    }
}
