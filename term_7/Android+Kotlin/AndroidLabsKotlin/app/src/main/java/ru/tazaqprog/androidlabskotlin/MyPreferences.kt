package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.os.PersistableBundle
import android.preference.PreferenceActivity

@SuppressLint("Registered")
class MyPreferences: PreferenceActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onBackPressed() {
        intent.putExtra("url", intent?.getStringExtra("url").toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}