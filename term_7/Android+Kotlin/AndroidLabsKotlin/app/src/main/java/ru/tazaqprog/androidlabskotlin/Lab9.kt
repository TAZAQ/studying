package ru.tazaqprog.androidlabskotlin

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lab9.*
import java.io.File


class Lab9 : AppCompatActivity() {

    var file: File? = null
    var directory: File? = null
    val PHOTO = 1
    val VIDEO = 2
    val REQUEST_PHOTO = 1
    val REQUEST_VIDEO = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab9)

        createDirectory()
    }

    private fun createDirectory() {
        directory = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            "MyFolder"
        )
        if (!directory!!.exists()) directory!!.mkdirs()
    }

    private fun generateFileUri(type: Int): Uri? {
        if (Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED) return null
        val path = File(Environment.getDataDirectory(), "CameraTest")
        if (!path.exists()) {
            if (!path.mkdirs()) {
                return null
            }
        }

        val newFile: File = when (type) {
            PHOTO -> File(path.path + File.separator + System.currentTimeMillis() + ".jpg")
            else -> File(path.path + File.separator + System.currentTimeMillis() + ".mp4")
        }
        return Uri.fromFile(newFile)
    }


    fun lab9ButtonsClick(view: View) {
        when (view.id) {
            R.id.aboutcamera -> {
                val intent = Intent(this, Lab9_aboutCamera::class.java)
                startActivity(intent)
            }
            R.id.startphoto -> {
                val intent = Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA)
                startActivity(intent)
            }
            R.id.startvideo -> {
                val intent = Intent(MediaStore.INTENT_ACTION_VIDEO_CAMERA)
                startActivity(intent);
            }
            R.id.caprutephoto -> {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val photoUri = generateFileUri(PHOTO)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(intent, REQUEST_PHOTO)
            }
            R.id.capturevideo -> {
                val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, generateFileUri(VIDEO))
                startActivityForResult(intent, REQUEST_VIDEO)
            }
            R.id.mycamera -> {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_PHOTO) {
            if (data != null) {
                if (data.hasExtra("data")) {
                    val bitmap: Bitmap = data.getParcelableExtra("data")
                    lab9_imageView.setImageBitmap(bitmap)
                }
            }
            else {
                lab9_imageView.setImageURI(Uri.fromFile(file))
            }
        }
    }
}
