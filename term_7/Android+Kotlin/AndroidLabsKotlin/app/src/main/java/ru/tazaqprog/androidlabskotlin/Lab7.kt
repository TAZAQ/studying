package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TabHost
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.content_lab7.*


class Lab7 : AppCompatActivity() {
    val TABLE_NAME = "mytask"
    val TASK_NAME = "task_name"
    val TASK_DONE = "task_done"

    lateinit var tabs: TabHost
    var mDatabaseHelper: DBHelper? = null
    var mSqLiteDatabase: SQLiteDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab7)

        tabs = tabhost
        tabs.setup()

        //первая вкладка
        var spec = tabs.newTabSpec("tag1")
        spec.setContent(R.id.tab1)
        spec.setIndicator("Не выполнено")
        tabs.addTab(spec)

        //вторая вкладка
        spec = tabs.newTabSpec("tag2")
        spec.setContent(R.id.tab2)
        spec.setIndicator("Выполнено")
        tabs.addTab(spec)

        //выводим на передний план первую вкладку
        tabs.currentTab = 0

        registerForContextMenu(isNotDoneList)
        registerForContextMenu(isDoneList)

        isNotDoneList.onItemSelectedListener = object: OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }

        isDoneList.onItemSelectedListener = object: OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

        myList()
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        when (v!!.id) {
            R.id.isDoneList -> menuInflater.inflate(R.menu.is_done_context_menu, menu)
            R.id.isNotDoneList -> menuInflater.inflate(R.menu.is_not_done_context_menu, menu)
        }
        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info: AdapterView.AdapterContextMenuInfo = item.menuInfo as AdapterView.AdapterContextMenuInfo
        var task = ""
        when (item.itemId) {
            R.id.executeTask -> {
                task = isNotDoneList.getItemAtPosition(info.position) as String
                ExecuteTask(task)
            }
            R.id.changeTask -> {
                task = isNotDoneList.getItemAtPosition(info.position) as String
                ChangeTask(task)
            }
            R.id.deleteTask -> {
                task = isNotDoneList.getItemAtPosition(info.position) as String
                DeleteTask(task)
            }
            R.id.clearTasks -> {
                clearTasks("0")
            }
            R.id.cancelTask2 -> {
                task = isDoneList.getItemAtPosition(info.position) as String
                CancelTask(task)
            }
            R.id.deleteTask2 -> {
                task = isDoneList.getItemAtPosition(info.position) as String
                DeleteTask(task)
            }
            R.id.clearTasks2 -> {
                clearTasks("1")
            }
        }

        return super.onContextItemSelected(item)
    }

    private fun myList() {
        mDatabaseHelper = DBHelper(this, "mydatabase.db", null, 1)

        try {
            mSqLiteDatabase = mDatabaseHelper!!.writableDatabase
        }
        catch (ex: SQLiteException) {
            mSqLiteDatabase = mDatabaseHelper!!.readableDatabase
        }

        val cursor: Cursor = mSqLiteDatabase!!.rawQuery("select * from $TABLE_NAME", null)

        val task = ArrayList<String>()
        val task2 = ArrayList<String>()
        val adapter: ArrayAdapter<String>
        adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1, task
        )
        val adapter2: ArrayAdapter<String>
        adapter2 = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1, task2
        )
        isNotDoneList.setAdapter(adapter)
        isDoneList.setAdapter(adapter2)
        if (cursor.moveToFirst()) {
            do {
                val id: Int = cursor.getInt(cursor.getColumnIndex(mDatabaseHelper!!.TASK_ID_COLUMN))
                val taskname: String =
                    cursor.getString(cursor.getColumnIndex(mDatabaseHelper!!.TASK_NAME_COLUMN))
                val taskdone: Int = cursor.getInt(cursor.getColumnIndex(mDatabaseHelper!!.TASK_DONE_COLUMN))
                if (taskdone == 0) {
                    task.add(0, taskname)
                    adapter.notifyDataSetChanged()
                } else {
                    task2.add(taskname)
                    adapter2.notifyDataSetChanged()
                }
            } while (cursor.moveToNext())
        } else Toast.makeText(this, "задач нет", Toast.LENGTH_SHORT).show()
        cursor.close()
    }

    @SuppressLint("InflateParams")
    fun addTaskClick(view: View) {
        val labIntent = Intent(this, Lab7_db_work_with_task::class.java)
        labIntent.putExtra("Action", "Add")
        startActivityForResult(labIntent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        myList()
    }

    //ВЫПОЛНИТЬ ЗАДАЧУ
    fun ExecuteTask(mtask: String) {
        mDatabaseHelper!!.changeDone(mtask, "1")
        Toast.makeText(this, "Задача $mtask выполнена!", Toast.LENGTH_LONG).show()
        myList()
    }

    //ОТМЕНИТЬ ВЫПОЛНЕНИЕ ЗАДАЧИ
    fun CancelTask(mtask: String) {
        mDatabaseHelper!!.changeDone(mtask, "0")
        Toast.makeText(this, "Задача $mtask не выполнена!", Toast.LENGTH_LONG).show()
        myList()
    }

    //УДАЛИТЬ ЗАДАЧУ
    fun DeleteTask(mtask: String) {
        mDatabaseHelper!!.deleteTask(mtask)
        Toast.makeText(this, "Задача $mtask удалена!", Toast.LENGTH_LONG).show()
        myList()
    }

    //ИЗМЕНИТЬ ЗАДАЧУ
    fun ChangeTask(mtask: String) {
        val labIntent = Intent(this, Lab7_db_work_with_task::class.java)
        labIntent.putExtra("Action", "Change")
        labIntent.putExtra("Task", mtask)
        startActivityForResult(labIntent, 1)
    }

    fun clearTasks(done: String) {
        mDatabaseHelper!!.clearTasks(done)
        Toast.makeText(this, "Задачи удалены!", Toast.LENGTH_LONG).show()
        myList()
    }

}
