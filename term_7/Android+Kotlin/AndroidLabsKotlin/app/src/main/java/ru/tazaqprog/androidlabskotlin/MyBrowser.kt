package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_my_browser.*


class MyBrowser : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_browser)

        myWebView.settings.javaScriptEnabled = true
        myWebView.webViewClient = WebViewClient()
        val uri: Uri? = intent.data
        myWebView.loadUrl(uri.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_browser, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun btnLoadClick(view: View) {
        myWebView.settings.javaScriptEnabled = true
        myWebView.loadUrl(text_url.text.toString())
    }

    override fun onResume() {
        super.onResume()
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val url = preferences.getString("url", "https://ya.ru")
        //myWebView.loadUrl(url)
        val allowImage = preferences.getBoolean("image", true)
        val allowJavaScript = preferences.getBoolean("javascript", true)
        val allowPopup = preferences.getBoolean("popup", false)
        val settings: WebSettings = myWebView.getSettings()
        settings.blockNetworkImage = allowImage
        settings.javaScriptEnabled = allowJavaScript
        settings.javaScriptCanOpenWindowsAutomatically = allowPopup
        text_url.setText(url)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.refresh -> myWebView.reload()
            R.id.back -> if (myWebView.canGoBack()) myWebView.goBack()
            R.id.forward -> if (myWebView.canGoForward()) myWebView.goForward()
            R.id.settings -> {
                val intent: Intent = Intent()
                intent.setClass(this, MyPreferences::class.java)
                intent.putExtra("url", text_url.text.toString())
                startActivityForResult(intent, 1)
            }
            R.id.exit -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (myWebView.canGoBack()) myWebView.goBack()
    }
}
