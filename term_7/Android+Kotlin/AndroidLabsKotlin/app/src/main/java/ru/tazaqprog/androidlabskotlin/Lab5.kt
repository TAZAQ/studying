package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.BatteryManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Vibrator
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class Lab5 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab5)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemMemory -> showRamMemory()
            R.id.itemScreenSizes -> showScreenSizes()
            R.id.itemBattery -> showBatteryLevel()
            R.id.itemBatteryHP -> showBatteryHealth()
            R.id.itemVibration -> doVibrate()

            R.id.itemOpenBrowser -> openOuterApp("browser")
            R.id.itemOpenContacts -> openOuterApp("call")
            R.id.itemOpenCamera -> openOuterApp("camera")
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showToast(text: String, len: Int = Toast.LENGTH_SHORT) {
        val toast = Toast.makeText(this, text, len)
        toast.show()
    }

    @SuppressLint("NewApi")
    private fun showRamMemory() {
        try {
            val availMemory = Runtime.getRuntime().totalMemory() / 1048576L
            showToast("Всего: $availMemory ГБ")
        } catch (e: Exception) {
            showToast("Не могу получить информацию о памяти устройства!")
        }
    }

    private fun showScreenSizes() {
        val thisFuckingDisplayMetrics = resources.displayMetrics
        val screenWidth = thisFuckingDisplayMetrics.widthPixels.toString()
        val screenHeight = thisFuckingDisplayMetrics.heightPixels.toString()
        showToast("$screenWidth px * $screenHeight px")
    }

    private fun showBatteryLevel() {
        val ifilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val battery = registerReceiver(null, ifilter)

        if (battery != null) {
            val batteryLevel = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1).toString()
            showToast("Заряд аккумулятора = $batteryLevel %")
        }
    }

    private fun showBatteryHealth() {
        val ifilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val battery = registerReceiver(null, ifilter)

        if (battery != null) {
            val batteryState = when (battery.getIntExtra(
                BatteryManager.EXTRA_HEALTH, -1)) {
                2 -> "Жива"
                3 -> "Перегрета"
                4 -> "Мертва"
                5 -> "Перегружена"
                6 -> "Ошибка получения информации"
                7 -> "Замерзла"
                else -> "ХЗ"
            }
            showToast("Здоровье аккумулятора - $batteryState")
        }
    }

    private fun doVibrate() {
        val vibration: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibration.vibrate(100)
    }

    private fun openOuterApp(app: String = "") {
        val intent = Intent()
        when (app) {
            "browser" -> {
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("https://ya.ru")
            }
            "call", "dial" -> {
                intent.action = Intent.ACTION_DIAL
            }
            "camera" -> {
                intent.action = MediaStore.ACTION_IMAGE_CAPTURE
            }
        }

        startActivity(intent)
    }
}
