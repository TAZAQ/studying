package ru.tazaqprog.androidlabskotlin

import android.hardware.Camera
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lab9_about_camera.*


class Lab9_aboutCamera : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab9_about_camera)

        val camera = Camera.open()
        val parameters = camera.parameters

        fillSupportedPictureSizes(parameters)
        fillSupportedFlashModes(parameters)
        fillSupportedAntibanding(parameters)
        fillSupportedPreviewSizes(parameters)
        fillSupportedJpegThumbnailSizes(parameters)
        fillSupportedPreviewFpsRange(parameters)
        fillSupportedFocusModes(parameters)
        fillSupportedPreviewFrameRates(parameters)
        fillSupportedColorEffects(parameters)
        fillSupportedSceneModes(parameters)
        fillSupportedVideoSizes(parameters)
        fillSupportedWhiteBalance(parameters)

        camera.release()

    }

    // поддерживаемые размеры изображения
    fun fillSupportedPictureSizes(parameters: Camera.Parameters) {
        val sizes = parameters.supportedPictureSizes
        if (sizes != null) {
            lab9_tv.append("Поддерживаемые форматы изображения: \n")
            for (size in sizes) {
                lab9_tv.append(String.format("%dx%d; ", size.height, size.width))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых фоматов изображений")
        }
    }

    // поддерживаемые режимы вспышки
    fun fillSupportedFlashModes(parameters: Camera.Parameters) {
        val flashModes = parameters.supportedFlashModes
        if (flashModes != null) {
            lab9_tv.append("\n\nПоддерживаемые режимы вспышки: \n")
            for (flashMode in flashModes) {
                lab9_tv.append(String.format("%s; ", flashMode))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых режимов вспышки")
        }
    }

    // поддерживаемый Антибандинг
    fun fillSupportedAntibanding(parameters: Camera.Parameters) {
        val antibandings = parameters.supportedAntibanding
        if (antibandings != null) {
            lab9_tv.append("\n\nПоддерживаемые режимы антибандинга: \n")
            for (antibanding in antibandings) {
                lab9_tv.append(String.format("%s; ", antibanding))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых режимов антибандинга")
        }
    }

    // поддерживаемые размеры превью
    fun fillSupportedPreviewSizes(parameters: Camera.Parameters) {
        val previewSizes = parameters.supportedPreviewSizes
        if (previewSizes != null) {
            lab9_tv.append("\n\nПоддерживаемые форматы изображения: \n")
            for (previewSize in previewSizes) {
                lab9_tv.append(String.format("%dx%d; ", previewSize.height, previewSize.width))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых фоматов изображений")
        }
    }

    // поддерживаемые размеры jpeg
    fun fillSupportedJpegThumbnailSizes(parameters: Camera.Parameters) {
        val sizes = parameters.supportedJpegThumbnailSizes
        if (sizes != null) {
            lab9_tv.append("\n\nПоддерживаемые форматы размера jpeg: \n")
            for (size in sizes) {
                lab9_tv.append(String.format("%dx%d; ", size.height, size.width))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых фоматов размера jpeg")
        }
    }

    // поддерживаемый диапазон превью FPS
    fun fillSupportedPreviewFpsRange(parameters: Camera.Parameters) {
        val fpsRanges = parameters.supportedPreviewFpsRange
        if (fpsRanges != null) {
            lab9_tv.append("\n\nПоддерживаемые диапазон превью FPS: \n")
            for (fpsRange in fpsRanges) {
                lab9_tv.append(String.format("%s; ", fpsRange[0]))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых диапазонов превью")
        }
    }

    // поддерживаемые режимы фокуса
    fun fillSupportedFocusModes(parameters: Camera.Parameters) {
        val focusModes = parameters.supportedFocusModes
        if (focusModes != null) {
            lab9_tv.append("\n\nПоддерживаемые режимы фокусировки: \n")
            for (focusMode in focusModes) {
                lab9_tv.append(String.format("%s; ", focusMode))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых режимов фокусировки")
        }
    }

    // поддерживаемый диапазон превью framerate
    fun fillSupportedPreviewFrameRates(parameters: Camera.Parameters) {
        val frameRates = parameters.supportedPreviewFrameRates
        if (frameRates != null) {
            lab9_tv.append("\n\nПоддерживаемые диапазон превью framerate: \n")
            for (framerate in frameRates) {
                lab9_tv.append(String.format("%s; ", framerate))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых диапазонов превью framerate")
        }
    }

    // поддерживаемые цветовые эффекты
    fun fillSupportedColorEffects(parameters: Camera.Parameters) {
        val colorEffects = parameters.supportedColorEffects
        if (colorEffects != null) {
            lab9_tv.append("\n\nПоддерживаемые цветовые эффекты: \n")
            for (colorEffect in colorEffects) {
                lab9_tv.append(String.format("%s; ", colorEffect))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых цветовых эффектов")
        }
    }

    // поддерживаемые режимы сцены
    fun fillSupportedSceneModes(parameters: Camera.Parameters) {
        val sceneModes = parameters.supportedSceneModes
        if (sceneModes != null) {
            lab9_tv.append("\n\nПоддерживаемые режимы сцены: \n")
            for (sceneMode in sceneModes) {
                lab9_tv.append(String.format("%s; ", sceneMode))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых режимов сцены")
        }
    }

    // поддерживаемые размеры видео
    fun fillSupportedVideoSizes(parameters: Camera.Parameters) {
        val sizes = parameters.supportedVideoSizes
        if (sizes != null) {
            lab9_tv.append("\n\nПоддерживаемые форматы видео: \n")
            for (size in sizes) {
                lab9_tv.append(String.format("%dx%d; ", size.height, size.width))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых фоматов видео")
        }
    }

    // поддерживаемые режимы баланса белого
    fun fillSupportedWhiteBalance(parameters: Camera.Parameters) {
        val whiteBalanceModes = parameters.supportedWhiteBalance
        if (whiteBalanceModes != null) {
            lab9_tv.append("\n\nПоддерживаемые режимы баланса белого: \n")
            for (mode in whiteBalanceModes) {
                lab9_tv.append(String.format("%s; ", mode))
            }
        }
        else {
            lab9_tv.append("\nНет поддерживаемых режимов баланса белого")
        }
    }
}
