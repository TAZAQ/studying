package ru.tazaqprog.androidlabskotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onLabClick(view: View) {

        val labIntent = when (view.id) {
            R.id.lab1Button -> Intent(this, Lab1::class.java)
            R.id.lab2Button -> Intent(this, Lab2::class.java)
            R.id.lab3Button -> Intent(this, Lab3::class.java)
            R.id.lab4Button -> Intent(this, Lab4::class.java)
            R.id.lab5Button -> Intent(this, Lab5::class.java)
            R.id.lab6Button -> Intent(this, Lab6MyLoginActivity::class.java)
            R.id.lab7Button -> Intent(this, Lab7::class.java)
            R.id.lab8Button -> Intent(this, Lab8::class.java)
            R.id.lab9Button -> Intent(this, Lab9::class.java)
            else -> Intent(this, Lab1::class.java)
        }

        startActivity(labIntent)
    }
}
