package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lab4.*

class Lab4 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab4)
        spinnersInit()
    }

    private fun spinnersInit() {
        backgroundSpinner.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                backgroundSpinnerItemSelected(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>){}
        }

        textColorSpinner.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                textColorSpinnerItemSelected(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>){}
        }
        buttonStyleSpinner.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                buttonStyleSpinnerItemSelected(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>){}
        }
    }

    @SuppressLint("NewApi")
    fun backgroundSpinnerItemSelected(itemIndex: Int) {
        iButton.text = "Я КНОПКА"
        when (itemIndex) {
            1 -> iButton.setBackgroundColor(getColor(R.color.colorBackgroundBlue))
            2 -> iButton.setBackgroundColor(getColor(R.color.colorBackgroundGold))
            3 -> {iButton.setBackgroundResource(R.drawable.tarakan); iButton.text = "Я ТОРОКАН" }
            else -> iButton.setBackgroundColor(getColor(R.color.colorBackgroundWhite))
        }
    }

    @SuppressLint("NewApi")
    fun textColorSpinnerItemSelected(itemIndex: Int) {
        val color = when (itemIndex) {
            1 -> R.color.colorTextBlue
            2 -> R.color.colorTextRed
            else -> R.color.colorTextBlack
        }
        iButton.setTextColor(getColor(color))
    }

    @SuppressLint("ResourceType")
    fun buttonStyleSpinnerItemSelected(itemIndex: Int) {
        when (itemIndex) {
            1 -> iButton.setBackgroundResource(R.style.buttonStyleRounded)
            2 -> iButton.setBackgroundResource(R.style.buttonStyleEllipse)
//            else -> iButton.setBackgroundResource(R.style.buttonStyleNormal)
        }
    }
}
