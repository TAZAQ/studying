package ru.tazaqprog.androidlabskotlin

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class DBHelper(
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version), BaseColumns {
    val TASK_NAME_COLUMN = "task_name"
    val TASK_DONE_COLUMN = "task_done"
    val TASK_ID_COLUMN = "id"
    private val DATABASE_NAME = "mydatabase.db"
    private val DATABASE_VERSION = 1
    val DATABASE_TABLE = "mytask"

    private val DATABASE_CREATE_SCRIPT = "create table $DATABASE_TABLE ($TASK_ID_COLUMN integer primary key autoincrement, $TASK_NAME_COLUMN text not null, $TASK_DONE_COLUMN  numeric);"
    private val DATABASE_DROP_SCRIPT = "DROP TABLE IF EXISTS $DATABASE_TABLE"

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DATABASE_CREATE_SCRIPT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(DATABASE_DROP_SCRIPT)
        onCreate(db);
    }

    @SuppressLint("Recycle")
    fun addTask(task: String, isDone: String) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TASK_NAME_COLUMN, task)
        values.put(TASK_DONE_COLUMN, isDone)
        db.insert(DATABASE_TABLE, null, values)
        db.close()
    }

    fun deleteTask(task: String) {
        val db = this.writableDatabase
        db.delete(DATABASE_TABLE, "$TASK_NAME_COLUMN=?", arrayOf(task))
        db.close()
    }

    fun changeDone(task: String, done: String) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TASK_NAME_COLUMN, task)
        values.put(TASK_DONE_COLUMN, done)
        db.update(DATABASE_TABLE, values, "$TASK_NAME_COLUMN=?", arrayOf(task))
        db.close()
    }

    fun changeTaskName(old: String, new: String) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TASK_NAME_COLUMN, new)
        db.update(DATABASE_TABLE, values, "$TASK_NAME_COLUMN=?", arrayOf(old))
        db.close()
    }

    fun clearTasks(done: String) {
        val db = this.writableDatabase
        db.delete(DATABASE_TABLE, "$TASK_DONE_COLUMN=?", arrayOf(done))
        db.close()
    }



}