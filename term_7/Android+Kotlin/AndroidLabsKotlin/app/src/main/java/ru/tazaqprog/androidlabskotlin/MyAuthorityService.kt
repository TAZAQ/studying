package ru.tazaqprog.androidlabskotlin

import android.content.Context
import android.content.SharedPreferences
import android.os.Parcel
import android.os.Parcelable

class MyAuthorityService(var mySettings: SharedPreferences, var myEditor: SharedPreferences.Editor) {

    private val APP_SETTINGS_LOGIN = "login"
    private val APP_SETTINGS_PASS = "password"

    private lateinit var login: String
    private lateinit var password: String

    init {
        initFields()
    }

    private fun initFields() {
        this.login = mySettings.getString(APP_SETTINGS_LOGIN, "").toString()
        this.password = mySettings.getString(APP_SETTINGS_PASS, "").toString()
    }

    fun loginPasswordIsNotExists(login: String, password: String) {
        if (
            mySettings.getString(APP_SETTINGS_LOGIN, null) == null ||
            mySettings.getString(APP_SETTINGS_PASS, null) == null
        ) {
            myEditor.putString(APP_SETTINGS_LOGIN, login)
            myEditor.putString(APP_SETTINGS_PASS, password)
            myEditor.apply()
        }
    }

    fun checkLoginPassword(login: String, password: String): Boolean {
        return this.login == login && this.password == password
    }

    fun changePassword(
        oldPassword: String,
        newPassword: String,
        newPassword2: String
    ): String {
        return if (newPassword.length < 5) "Длина пароля меньше 5 символов"
            else if (this.password == oldPassword)
            if (newPassword == newPassword2) {
                doChangePassword(newPassword)
                "Пароль успешно изменён"
            }
            else "Новые пароли не совпадают"
        else "Неверный старый пароль"
    }

    private fun doChangePassword(newPassword: String) {
        myEditor.putString(APP_SETTINGS_PASS, newPassword)
        myEditor.apply()
    }
}