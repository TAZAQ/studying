package ru.tazaqprog.androidlabskotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_lab1.*

class Lab1 : AppCompatActivity() {

    var tarakanCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab1)
    }

    fun addTarakanClick(view: View) {
        tarakanCounter++
        tarakanCountView.text = tarakanCounter.toString()
    }

    fun tarakanCountResetClick(view: View) {
        tarakanCounter = 0
        tarakanCountView.text = tarakanCounter.toString()
    }

}
