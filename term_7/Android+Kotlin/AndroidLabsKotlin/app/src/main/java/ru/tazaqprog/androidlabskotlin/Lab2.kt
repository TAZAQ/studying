package ru.tazaqprog.androidlabskotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_lab2.*

class Lab2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab2)
    }

    fun sendForm(view: View) {
        loginEdit.setText("")
        emailEdit.setText("")
        dobEdit.setText("")
        sexEdit.setText("")
        passwordEdit.setText("")
        passwordREdit.setText("")

        val toast = Toast.makeText(this, "Отправлено", Toast.LENGTH_SHORT)
        toast.show()
    }
}
