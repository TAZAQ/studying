package ru.tazaqprog.androidlabskotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_login.*

class Lab6MyLoginActivity : AppCompatActivity() {
    private val APP_SETTINGS = "password"

    var passwordFailsCounter = 1;

    lateinit var mySettings: SharedPreferences
    lateinit var myEditor: SharedPreferences.Editor

    lateinit var myAuth: MyAuthorityService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_login)



        mySettingsInit()

        myAnimation()
    }

    private fun myAnimation() {
        val authorAnim = AnimationUtils.loadAnimation(this, R.anim.avt_anim)
        val btnExitAnim = AnimationUtils.loadAnimation(this, R.anim.tv_enter)
        val loginPasswordAnim = AnimationUtils.loadAnimation(this, R.anim.et_enter)
        val btnEnterAnim = AnimationUtils.loadAnimation(this, R.anim.btn_enter)

        tvAuthor.startAnimation(authorAnim)
        editLogin.startAnimation(loginPasswordAnim)
        editPassword.startAnimation(loginPasswordAnim)
        btnEnter.startAnimation(btnEnterAnim)
        btnExit.startAnimation(btnExitAnim)
    }

    // инициализация SharedPreferences
    fun mySettingsInit() {
        mySettings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE)
        myEditor = mySettings.edit()

        myAuth = MyAuthorityService(mySettings, myEditor)
    }

    // обработчик нажатий кнопок
    fun lab6LoginButtonsClick(view: View) {
        when (view.id) {
            R.id.btnEnter -> checkUserLoginPassword(
                editLogin.text.toString(),
                editPassword.text.toString()
            )
            R.id.btnExit -> finish()
        }
    }

    private fun checkUserLoginPassword(login: String, password: String) {
        myAuth.loginPasswordIsNotExists(login, password)
        if (myAuth.checkLoginPassword(login, password)) {
            editLogin.text.clear()

            val navIntent = Intent(this, Lab6::class.java)
            navIntent.putExtra(getString(R.string.tryCounter), passwordFailsCounter)
            startActivity(navIntent)

            finish()
        }
        else {
            Toast.makeText(this, "Неверный логин или пароль", Toast.LENGTH_SHORT).show()
            passwordFailsCounter++
        }

        editPassword.text.clear()
    }

}